import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { MainPageData as MainPageDataSchema } from '@/types/MainPageData';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

import { errorStore } from '@/store';

@Module({
    name: 'MainPage',
    stateFactory: true,
    namespaced: true,
})
export default class MainPage extends VuexModule {
    mainPage: MainPageDataSchema = {} as MainPageDataSchema;

    get MAIN_PAGE(): MainPageDataSchema {
        return this.mainPage;
    }

    @Mutation
    SET_MAIN_PAGE(data: MainPageDataSchema): void {
        this.mainPage = data;
    }

    @Action({ rawError: true })
    async LOAD_MAIN_PAGE(): Promise<void> {
        try {
            let data: any = await axios.get(apiUrl + `pages/main`);
            this.SET_MAIN_PAGE(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async EDIT_MAIN_PAGE(data: any): Promise<void> {
        try {
            await axios.put(`${apiUrl}pages/main`, data);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }
}
