import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Layout as LayoutSchema } from 'types/layout';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

import { errorStore } from '@/store';

@Module({
    name: 'Layout',
    stateFactory: true,
    namespaced: true,
})
export default class Layout extends VuexModule {
    private layout: LayoutSchema = {} as LayoutSchema;

    get LAYOUT(): LayoutSchema {
        return this.layout;
    }

    @Mutation
    SET_LAYOUT(layout: LayoutSchema) {
        this.layout = layout;
    }

    @Action({ rawError: true })
    async LOAD_LAYOUT(): Promise<void> {
        try {
            let data: any = await axios.get(`${apiUrl}layout`);
            this.SET_LAYOUT(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async EDIT_LAYOUT(layout: LayoutSchema): Promise<void> {
        try {
            let data: any = await axios.put(`${apiUrl}layout`, layout);
            this.SET_LAYOUT(data.data);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }
}
