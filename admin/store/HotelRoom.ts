import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { HotelRoom as HotelRoomSchema } from 'types/hotelRoom';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

import { errorStore } from '@/store';

@Module({
    name: 'HotelRoom',
    stateFactory: true,
    namespaced: true,
})
export default class HotelRoom extends VuexModule {
    private hotelRooms: HotelRoomSchema[] = [];

    get HOTEL_ROOMS(): HotelRoomSchema[] {
        return this.hotelRooms;
    }

    @Mutation
    SET_HOTEL_ROOMS(hotelRooms: HotelRoomSchema[]) {
        this.hotelRooms = hotelRooms;
    }

    @Action({ rawError: true })
    async LOAD_HOTEL_ROOMS(): Promise<void> {
        try {
            let data: any = await axios.get(`${apiUrl}hotelRoom`);
            this.SET_HOTEL_ROOMS(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async ADD_HOTEL_ROOM(hotelRoom: any): Promise<void> {
        try {
            await axios.post(`${apiUrl}hotelRoom`, hotelRoom);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }

    @Action({ rawError: true })
    async EDIT_HOTEL_ROOM(hotelRoom: any): Promise<void> {
        try {
            await axios.put(`${apiUrl}hotelRoom`, hotelRoom);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }

    @Action({ rawError: true })
    async DELETE_HOTEL_ROOM(hotelRoomId: number): Promise<void> {
        try {
            await axios.delete(`${apiUrl}hotelRoom?id=${hotelRoomId}`);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }
}
