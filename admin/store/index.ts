import { Store } from 'vuex';
import { getModule } from 'vuex-module-decorators';
import Layout from '~/store/Layout';
import Error from '~/store/Error';
import MainPage from '~/store/MainPage';
import Meta from '~/store/Meta';
import Contact from '~/store/Contact';
import HotelRoom from '~/store/HotelRoom';
import User from '~/store/User';
import Loading from '~/store/Loading';

let layoutStore: Layout;
let errorStore: Error;
let mainPageStore: MainPage;
let metaStore: Meta;
let contactStore: Contact;
let hotelRoomStore: HotelRoom;
let userStore: User;
let loadingStore: Loading;

const initializer = (store: Store<any>) => {
    layoutStore = getModule(Layout, store);
    errorStore = getModule(Error, store);
    mainPageStore = getModule(MainPage, store);
    metaStore = getModule(Meta, store);
    contactStore = getModule(Contact, store);
    hotelRoomStore = getModule(HotelRoom, store);
    userStore = getModule(User, store);
    loadingStore = getModule(Loading, store);
};

export const plugins = [initializer];

export {
    layoutStore,
    errorStore,
    mainPageStore,
    metaStore,
    contactStore,
    hotelRoomStore,
    userStore,
    loadingStore,
};
