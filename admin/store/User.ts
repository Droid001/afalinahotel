import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { User as UserSchema } from '@/types/user';
import axios from '~/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;
import { setUserData, deleteUserData } from '@/helpers/localStorageHelpers';

@Module({
    name: 'User',
    stateFactory: true,
    namespaced: true,
})
export default class User extends VuexModule {
    private user: UserSchema = {} as UserSchema;

    get USER(): UserSchema {
        return this.user;
    }

    @Mutation
    SET_USER(user: UserSchema) {
        this.user = user;
    }

    @Mutation
    RESET_USER(): void {
        this.user = {
            status: 0,
            token: '',
            user: {
                id: 0,
                name: '',
                email: '',
                email_verified_at: null,
                created_at: '',
                updated_at: '',
            },
        };
    }

    @Action({ rawError: true })
    async AUTH(form: string[]): Promise<void> {
        try {
            let data = await axios.post(`${apiUrl}login`, {
                username: form[0],
                password: form[1],
            });
            if (data.data.status == 200) {
                this.SET_USER(data.data);
                setUserData(data.data);
            }
        } catch (e) {
            console.log(e);
        }
    }

    @Action({ rawError: true })
    async LOGOUT(): Promise<void> {
        try {
            await axios.post(`${apiUrl}logout`);
            deleteUserData();
            this.RESET_USER();
        } catch (e) {
            console.log(e);
        }
    }
}
