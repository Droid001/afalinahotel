import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { ErrorSchema } from '@/types/error';

@Module({
    name: 'Error',
    stateFactory: true,
    namespaced: true,
})
export default class Error extends VuexModule {
    private error: ErrorSchema = {
        status: false,
        title: '',
    };

    get ERROR(): ErrorSchema {
        return this.error;
    }

    @Mutation
    SET_ERROR(error: ErrorSchema) {
        this.error = error;
    }

    @Mutation
    RESET_ERROR() {
        this.error = {
            status: false,
            title: '',
        };
    }
}
