import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Contact as ContactSchema } from '@/types/contact';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

import { errorStore } from '@/store';

@Module({
    name: 'Contact',
    stateFactory: true,
    namespaced: true,
})
export default class Contact extends VuexModule {
    private contacts: ContactSchema[] = [];
    private activeContact: ContactSchema = {
        id: -1,
        title: '',
        text: '',
    };

    get CONTACTS(): ContactSchema[] {
        return this.contacts;
    }

    get ACTIVE_CONTACT(): ContactSchema {
        return this.activeContact;
    }

    @Mutation
    SET_CONTACTS(contacts: ContactSchema[]) {
        this.contacts = contacts;
    }

    @Mutation
    SET_ACTIVE_CONTACT(contact: ContactSchema) {
        this.activeContact = contact;
    }

    @Action({ rawError: true })
    async LOAD_CONTACTS(): Promise<void> {
        try {
            let data: any = await axios.get(`${apiUrl}contacts`);
            this.SET_CONTACTS(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async LOAD_CONTACT(id: number): Promise<void> {
        try {
            let data: any = await axios.get(`${apiUrl}contact?id=${id}`);
            this.SET_ACTIVE_CONTACT(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async ADD_CONTACT(contact: ContactSchema): Promise<void> {
        try {
            await axios.post(`${apiUrl}contacts`, contact);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }

    @Action({ rawError: true })
    async EDIT_CONTACT(contact: ContactSchema): Promise<void> {
        try {
            await axios.put(`${apiUrl}contacts`, contact);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }

    @Action({ rawError: true })
    async DELETE_CONTACT(id: number): Promise<void> {
        try {
            await axios.delete(`${apiUrl}contacts?id=${id}`);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }
}
