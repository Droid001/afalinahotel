import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Meta as MetaSchema } from '@/types/meta';
import axios from '@/helpers/axiosHelpers';

import { errorStore } from '@/store';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'Meta',
    stateFactory: true,
    namespaced: true,
})
export default class Contact extends VuexModule {
    private meta: MetaSchema[] = [];

    get META(): MetaSchema[] {
        return this.meta;
    }

    @Mutation
    SET_META(metas: MetaSchema[]) {
        this.meta = metas;
    }

    @Action({ rawError: true })
    async LOAD_META(): Promise<void> {
        try {
            let data: any = await axios.get(apiUrl + `meta`);
            this.SET_META(data.data);
        } catch (e) {
            errorStore.SET_ERROR({ status: false, title: String(e) });
        }
    }

    @Action({ rawError: true })
    async EDIT_META(data: any): Promise<void> {
        try {
            await axios.put(`${apiUrl}meta`, data);
            errorStore.SET_ERROR({
                status: true,
                title: 'Изменения сохранены',
            });
        } catch (e) {
            errorStore.SET_ERROR({
                status: false,
                title: 'Ошибка при сохранении данных, пожалуйста попробуйте позже',
            });
        }
    }
}
