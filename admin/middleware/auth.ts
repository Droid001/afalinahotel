import { userStore } from '@/store';
import { User as UserSchema } from '@/types/user';
import { getUserData } from '@/helpers/localStorageHelpers';
export default async function (context: any) {
    const isAuthenticated = userStore.USER.status === 200;
    if (isAuthenticated) {
        if (context.route.name == 'auth') {
            return;
        }
    } else {
        const user: UserSchema | null = getUserData();
        if (user !== null) {
            userStore.SET_USER(user);
            return;
        }

        if (context.route.name != 'auth') {
            context.redirect({ name: 'auth' });
        }
    }
}
