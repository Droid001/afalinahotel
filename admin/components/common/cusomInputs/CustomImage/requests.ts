import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;
//import { Image as ImageSchema } from "@/types/image";
import { errorStore } from '@/store';

export async function addImage(image: File): Promise<number[] | null> {
    let form = new FormData();
    form.append('image', image);

    try {
        let data: any = await axios.post(`${apiUrl}image`, form, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
        return data.data;
    } catch (e) {
        errorStore.SET_ERROR({ status: false, title: String(e) });
    }
    return null;
}

export async function deleteImages(ides: number[]): Promise<number[] | null> {
    try {
        await axios.delete(`${apiUrl}image?ides=${ides.join('+')}`);
    } catch (e) {
        errorStore.SET_ERROR({ status: false, title: String(e) });
    }
    return null;
}
