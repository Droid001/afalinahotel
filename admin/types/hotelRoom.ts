import { Image as ImageSchema } from './image';

export interface Feature {
    title: string;
    description: string;
    img: ImageSchema;
    newImageId?: number;
}

export interface Price {
    periud: string;
    value: string;
}

export interface HotelRoom {
    id: number;
    img: ImageSchema;
    title: string;
    intro: string;
    description: string;
    features: Feature[];
    pricies: Price[];
    busy_dates: number[];
    gallery: ImageSchema[];
    slides: ImageSchema[];
}
