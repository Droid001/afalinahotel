export interface MainPageData {
    title: string;
    slides: Array<string>;
    description: string;
}
