export interface User {
    token: string;
    user: {
        id: number;
        name: string;
        email: string;
        email_verified_at: any;
        created_at: string;
        updated_at: string;
    };
    status: number;
}
