export interface Link {
    url: string;
    name: string;
}

export interface Layout {
    navbar: {
        logo: Link;
        tel: string;
        links: Link[];
    };
}
