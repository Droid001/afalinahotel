export interface Image {
    id: number;
    src: string;
    file?: File;
}
