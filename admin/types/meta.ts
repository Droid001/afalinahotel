export interface Meta {
    name: string;
    title: string;
    description: string;
    keywords: string;
    robots: string;
}
