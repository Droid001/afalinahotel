export enum sidebarItemType {
    group = 'group',
    link = 'link',
}

export interface SidebarItem {
    type: sidebarItemType; //group or link
    level: number;
    title: string;
    routeName: string;
    childeren?: Array<SidebarItem>;
    description?: string;
}
