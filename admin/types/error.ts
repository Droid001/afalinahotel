export interface ErrorSchema {
    status: boolean;
    title: string;
}
