import { User as UserSchema } from '@/types/user';
import { userStore } from '@/store';
import User from '~/store/User';
const keys = {
    user: 'AfalinaAdminPanelUser',
    time: 'AfalinaAdminPanelUserDate',
};

const days: number = 3;

export function setUserData(user: UserSchema): void {
    if (userStore.USER.status !== 200) return;
    if (process.client) {
        window.localStorage.setItem(keys.user, JSON.stringify(userStore.USER));
        window.localStorage.setItem(keys.time, String(Date.now()));
    }
}

export function getUserData(): UserSchema | null {
    let json: string = '';
    let jsonDate: string = '';
    let user: UserSchema = {} as UserSchema;
    let isActive: boolean = false;
    let isUserOK: boolean = false;
    //Проверяем сроки
    jsonDate = window.localStorage.getItem(keys.time) || '';
    if (jsonDate) {
        isActive = !!((+Date.now() - +jsonDate) / 1000 / 60 / 60 / 24 <= days);
    }
    //Получаем юзера
    json = window.localStorage.getItem(keys.user) || '';
    if (json.length) {
        user = JSON.parse(json);
        if (user.status && user.status == 200) isUserOK = true;
    }
    if (isActive && isUserOK) {
        return user;
    } else {
        window.localStorage.removeItem(keys.user);
        window.localStorage.removeItem(keys.time);
        return null;
    }
}

export function deleteUserData(): void {
    window.localStorage.removeItem(keys.user);
    window.localStorage.removeItem(keys.time);
}
