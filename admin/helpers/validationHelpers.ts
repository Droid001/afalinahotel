export function validateName(name: string): boolean | null {
    if (!name) {
        return null;
    }
    if (name.length <= 2) {
        return false;
    }
    return true;
}

export function validateTel(tel: string): boolean | null {
    if (!tel) {
        return null;
    }
    if (tel.length <= 2 || tel[tel.length - 1] == '_') {
        return false;
    }
    return true;
}

export function validateMassage(message: string): boolean | null {
    if (!message) {
        return null;
    }
    if (message.length < 3) {
        return false;
    }
    return true;
}
//Seo forms

export function validateMetatitle(title: string): boolean | null {
    if (!title) {
        return null;
    }
    if (title.length <= 2) {
        return false;
    }
    if (title.length > 100) {
        return false;
    }
    return true;
}

export function validateBigText(text: string): boolean | null {
    if (!text) {
        return null;
    }
    if (text.length <= 2) {
        return false;
    }
    if (text.length > 2000) {
        return false;
    }
    return true;
}

export function validateMiddleText(text: string): boolean | null {
    if (!text) {
        return null;
    }
    if (text.length <= 2) {
        return false;
    }
    if (text.length > 500) {
        return false;
    }
    return true;
}
