import { Meta as MetaSchema } from '@/types/meta';

export function setMeta(
    meta: MetaSchema[],
    name: any,
    noName: boolean = false,
): any {
    let temp: any = null;
    if (noName) {
        temp = meta[0];
    } else {
        meta.forEach((elem: any) => {
            if (elem.name === name) {
                temp = elem;
            }
        });
    }
    if (!temp) {
        return;
    }

    let result: any = {};
    result.meta = [];
    if (temp.title) {
        result.title = temp.title;
    }

    if (temp.description) {
        result.meta.push({
            hid: 'description',
            name: 'description',
            content: temp.description,
        });
    }

    if (temp.keywords) {
        result.meta.push({
            hid: 'keywords',
            name: 'keywords',
            content: temp.keywords,
        });
    }
    if (temp.robots) {
        result.meta.push({
            hid: 'robots',
            name: 'robots',
            content: temp.robots,
        });
    }
    return result;
}
