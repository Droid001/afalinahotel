export interface Contact {
    id: number;
    title: string;
    text: string;
}
