import { Image as ImageSchema } from './image';

export interface Feature {
    title: string;
    description: string;
    img: ImageSchema;
}

export interface Price {
    periud: string;
    value: string;
}

export interface HotelRoom {
    id: number;
    img: ImageSchema;
    title: string;
    intro: string;
    description: string;
    features: Feature[];
    pricies: Price[];
    busyDates: number[];
    gallery: ImageSchema[];
    slides: ImageSchema[];
}
