import { Image as ImageSchema } from './image';

export interface MainPageData {
    title: string;
    slides: Array<ImageSchema>;
    description: string;
}
