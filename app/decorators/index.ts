import { createDecorator } from 'vue-class-component';

export const head: any = createDecorator((options: any, key: any) => {
    const originalMethod: any = options.methods[key];

    options.methods[key] = function wrapperMethod(...args: any) {
        // Print a log.
        console.log(`Invoked: ${key}(`, ...args, ')');

        // Invoke the original method.
        originalMethod.apply(this, args);
    };
});
