export default {
    ssr: true,

    env: {
        WEATHER_API_KEY: '641a69379dca5fcc1ea8a888c04234e3',
        BASE_API_URL: 'http://lk.yustocorp.local:8081/api/v1/',
    },

    loading: false,

    // Global page headers (https://go.nuxtjs.dev/config-head)
    head: {
        title: 'Afalina hotel',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content: 'Гостевой дом Афалина',
            },
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: ['@/assets/scss/custom.scss'],

    styleResources: {
        scss: [],
    },

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [{ src: '@/plugins/vue-inputmask.ts', ssr: false }],

    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [
        // https://go.nuxtjs.dev/typescript
        '@nuxt/typescript-build',
        // https://go.nuxtjs.dev/stylelint
        //'@nuxtjs/stylelint-module',
    ],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/style-resources',
        'bootstrap-vue/nuxt',
    ],
    bootstrapVue: {
        bootstrapCSS: false,
        bootstrapVueCSS: false,
    },

    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: ['vue-style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {},

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {},
};
