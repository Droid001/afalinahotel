import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { MainPageData as MainPageDataSchema } from '@/types/MainPageData';
import { Image as ImageSchema } from '@/types/image';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'MainPage',
    stateFactory: true,
    namespaced: true,
})
export default class MainPage extends VuexModule {
    mainPage: MainPageDataSchema = {} as MainPageDataSchema;

    get BIG_CAROUSEL(): ImageSchema[] {
        if (!this.mainPage.slides) {
            return [];
        }
        return this.mainPage.slides;
    }

    get BANNER_TITLE(): string {
        return this.mainPage.title;
    }

    get DESCRIPTION(): string {
        return this.mainPage.description;
    }

    @Mutation
    SET_MAIN_PAGE(data: MainPageDataSchema): void {
        this.mainPage = data;
    }

    @Action({ rawError: true })
    async LOAD_MAIN_PAGE(): Promise<void> {
        let data: any = await axios.get(apiUrl + `pages/main`);
        this.SET_MAIN_PAGE(data.data);
    }
}
