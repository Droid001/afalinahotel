import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Layout as LayoutSchema } from 'types/layout';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'Layout',
    stateFactory: true,
    namespaced: true,
})
export default class Layout extends VuexModule {
    private layout: LayoutSchema = {} as LayoutSchema;

    get LAYOUT(): LayoutSchema {
        return this.layout;
    }

    @Mutation
    SET_LAYOUT(layout: LayoutSchema) {
        this.layout = layout;
    }

    @Action({ rawError: true })
    async LOAD_LAYOUT(): Promise<void> {
        let data: any = await axios.get(`${apiUrl}layout`);
        this.SET_LAYOUT(data.data);
    }
}
