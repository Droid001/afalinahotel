import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Contact as ContactSchema } from '@/types/contact';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'Contact',
    stateFactory: true,
    namespaced: true,
})
export default class Contact extends VuexModule {
    private contacts: ContactSchema[] = [];

    get CONTACTS(): ContactSchema[] {
        return this.contacts;
    }

    @Mutation
    SET_CONTACTS(contacts: ContactSchema[]) {
        this.contacts = contacts;
    }

    @Action({ rawError: true })
    async LOAD_CONTACTS(): Promise<void> {
        let data: any = await axios.get(`${apiUrl}contacts`);
        this.SET_CONTACTS(data.data);
    }
}
