import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';

@Module({
    name: 'Loading',
    stateFactory: true,
    namespaced: true,
})
export default class Loading extends VuexModule {
    private queries: number = 0;

    get QUERIES(): number {
        return this.queries;
    }

    @Mutation
    ADD() {
        ++this.queries;
    }

    @Mutation
    SUB() {
        if (this.queries > 0) --this.queries;
    }
}
