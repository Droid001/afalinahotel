import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { Weather as WeatherSchema } from 'types/weather';
import axios from '~/plugins/axios';

@Module({
    name: 'Weather',
    stateFactory: true,
    namespaced: true,
})
export default class Weather extends VuexModule {
    private weather: WeatherSchema = {} as WeatherSchema;

    @Action
    async LOAD_WEATHER(): Promise<void> {
        const data: any = await axios.get(
            `https://api.openweathermap.org/data/2.5/weather?q=anapa&appid=${process.env.WEATHER_API_KEY}&units=metric&lang=ru`,
        );
        this.SET_WEATHER(data.data);
    }

    @Mutation
    SET_WEATHER(weather: WeatherSchema): void {
        this.weather = weather;
    }

    get WEATHER(): WeatherSchema {
        return this.weather;
    }
}
