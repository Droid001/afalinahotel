import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { HotelRoom as HotelRoomSchema } from 'types/hotelRoom';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'Feedback',
    stateFactory: true,
    namespaced: true,
})
export default class Feedback extends VuexModule {
    @Action({ rawError: true })
    async SEND_FEEDACK(feedback: any): Promise<void> {
        try {
            await axios.post(`${apiUrl}feedback`, feedback);
        } catch (e) {}
    }
}
