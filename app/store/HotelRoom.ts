import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { HotelRoom as HotelRoomSchema } from 'types/hotelRoom';
import axios from '@/helpers/axiosHelpers';
const apiUrl: string | undefined = process.env.BASE_API_URL;

@Module({
    name: 'HotelRoom',
    stateFactory: true,
    namespaced: true,
})
export default class HotelRoom extends VuexModule {
    private hotelRooms: HotelRoomSchema[] = [];

    get HOTEL_ROOMS(): HotelRoomSchema[] {
        return this.hotelRooms;
    }

    @Mutation
    SET_HOTEL_ROOMS(hotelRooms: HotelRoomSchema[]) {
        this.hotelRooms = hotelRooms;
    }

    @Action({ rawError: true })
    async LOAD_HOTEL_ROOMS(): Promise<void> {
        try {
            let data: any = await axios.get(`${apiUrl}hotelRoom`);
            this.SET_HOTEL_ROOMS(data.data);
        } catch (e) {}
    }
}
