import { Store } from 'vuex';
import { getModule } from 'vuex-module-decorators';
import Layout from '~/store/Layout';
import Weather from '~/store/Weather';
import MainPage from '~/store/MainPage';
import HotelRoom from '~/store/HotelRoom';
import Contact from '~/store/Contact';
import Meta from '~/store/Meta';
import Feedback from '~/store/Feedback';
import Loading from '~/store/Loading';

let layoutStore: Layout;
let weatherStore: Weather;
let mainPageStore: MainPage;
let hotelRoomStore: HotelRoom;
let contactStore: Contact;
let metaStore: Meta;
let feedbackStore: Feedback;
let loadingStore: Loading;

const initializer = (store: Store<any>) => {
    layoutStore = getModule(Layout, store);
    weatherStore = getModule(Weather, store);
    mainPageStore = getModule(MainPage, store);
    hotelRoomStore = getModule(HotelRoom, store);
    contactStore = getModule(Contact, store);
    metaStore = getModule(Meta, store);
    feedbackStore = getModule(Feedback, store);
    loadingStore = getModule(Loading, store);
};

export const plugins = [initializer];

export {
    layoutStore,
    weatherStore,
    mainPageStore,
    hotelRoomStore,
    contactStore,
    metaStore,
    feedbackStore,
    loadingStore,
};
