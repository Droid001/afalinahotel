export function validateName(name: string): boolean | null {
    if (name.length == 0) {
        return null;
    }
    if (name.length <= 2) {
        return false;
    }
    return true;
}

export function validateTel(tel: string): boolean | null {
    if (tel.length == 0) {
        return null;
    }
    if (tel.length <= 2 || tel[tel.length - 1] == '_') {
        return false;
    }
    return true;
}

export function validateMassage(message: string): boolean | null {
    if (message.length == 0) {
        return null;
    }
    if (message.length < 3) {
        return false;
    }
    return true;
}
