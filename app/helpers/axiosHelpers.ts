import axios from '@/plugins/axios';
import { loadingStore } from '@/store';

function addToken(params: any) {
    // if (params.headers) {
    //     params.headers.Authorization = 'Bearer ' + userStore.USER.token;
    // }
    // else {
    //     params.headers = {
    //         Authorization: 'Bearer ' + userStore.USER.token
    //     }
    // }
}

export default {
    async get(url: string, params: any = {}): Promise<any> {
        addToken(params);
        loadingStore.ADD();
        const loaded = await axios.get(url, params);
        loadingStore.SUB();
        return loaded;
    },

    async post(url: string, data: any = null, params: any = {}): Promise<any> {
        addToken(params);
        loadingStore.ADD();
        const loaded = await axios.post(url, data, params);
        loadingStore.SUB();
        return loaded;
    },

    async put(url: string, data: any = null, params: any = {}): Promise<any> {
        addToken(params);
        loadingStore.ADD();
        const loaded = await axios.put(url, data, params);
        loadingStore.SUB();
        return loaded;
    },

    async delete(url: string, params: any = {}): Promise<any> {
        addToken(params);
        loadingStore.ADD();
        const loaded = axios.delete(url, params);
        loadingStore.SUB();
        return loaded;
    },
};
