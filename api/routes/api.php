<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HotelRoomController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LayoutController;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\MetaController;
use \App\Http\Controllers\FeedbackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Layout
Route::get('/v1/layout', [LayoutController::class, 'get']);
//Images
Route::get('/v1/image', [ImageController::class, 'get']);
//pages
//mainPage
Route::get('/v1/pages/main', [MainPageController::class, 'get']);
//SEO
Route::get('/v1/meta', [MetaController::class, 'get']);
//contacts
Route::get('/v1/contacts', [ContactController::class, 'get']);
Route::get('/v1/contact', [ContactController::class, 'getById']);
//hotel rooms
Route::get('/v1/hotelRoom', [HotelRoomController::class, 'get']);
//feedback
Route::post('/v1/feedback', [FeedbackController::class, 'get']);
//auth
Route::post('/v1/login', [AuthController::class, 'login']);
Route::middleware('auth:api')->group(function () {
    Route::post('/v1/register', [AuthController::class, 'register']);
    Route::post('/v1/logout', [AuthController::class, 'logout']);

//Layout
    Route::put('/v1/layout', [LayoutController::class, 'edit']);
//Images
    Route::post('/v1/image', [ImageController::class, 'add']);
    Route::delete('/v1/image', [ImageController::class, 'delete']);
//pages
//mainPage
    Route::put('/v1/pages/main', [MainPageController::class, 'edit']);
//SEO
    Route::put('/v1/meta', [MetaController::class, 'edit']);
//contacts
    Route::post('/v1/contacts', [ContactController::class, 'add']);
    Route::put('/v1/contacts', [ContactController::class, 'edit']);
    Route::delete('/v1/contacts', [ContactController::class, 'delete']);
//hotel rooms
    Route::post('/v1/hotelRoom', [HotelRoomController::class, 'add']);
    Route::put('/v1/hotelRoom', [HotelRoomController::class, 'edit']);
    Route::delete('/v1/hotelRoom', [HotelRoomController::class, 'delete']);

});
