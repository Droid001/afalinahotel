<?php

namespace App\Services;

class TelegramBot
{

    private static string $route = "https://api.telegram.org/";
    private static string $token = //"1507595663:AAFv_s52k9WVP1b6drxBswyFtcxEL32K7QY";
                                    "1083528209:AAF7FCRYbcLa6BxY3RWuhMMDhWf5EjNmutI";

    static private function getChatsIdes()
    {
        $url = TelegramBot::$route . 'bot' . TelegramBot::$token . '/getUpdates';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $chat_id_list = [];
        foreach ($response['result'] as $item) {
            array_push($chat_id_list, $item['message']['chat']['id']);
        }

        return $chat_id_list;
    }

    static private function sendMessages($chat_id_list, $message)
    {
        $response = [];
        foreach ($chat_id_list as $chat_id) {
            $url = TelegramBot::$route .'bot' . TelegramBot::$token.'/sendMessage';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                    "text":"' . $message . '",
                    "chat_id": "' . $chat_id . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

        }
        return $response;
    }

    static private function formatMessage($message): string
    {
        $formatedMessage = 'Тип: ' . $message->type . "\r\n" .
            'Имя: ' . $message->name . "\r\n" .
            'Телефон: ' . $message->tel . "\r\n" .
            'Дата: ' . $message->date . "\r\n";

        if ($message->type == 'Бронь') {
            $formatedMessage .= 'Тип Номера: ' . $message->hotelRoomType . "\r\n" .
                'Дата въезда: ' . $message->dateIn . "\r\n" .
                'Дата выезда: ' . $message->dateOut . "\r\n";
        } else {
            $formatedMessage .= 'Сообщение: ' . "\r\n" . $message->message . "\r\n";
        }

        return $formatedMessage;

    }

    static public function init($message)
    {
        $message = TelegramBot::formatMessage($message);
        $chat_ides = TelegramBot::getChatsIdes();
        return TelegramBot::sendMessages($chat_ides, $message);
    }
}

