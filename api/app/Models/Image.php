<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;

    public static function get_images($ides)
    {
        $result = [];
        foreach ($ides as $id) {
            $image = Image::find($id);
            if (!empty($image)) {
                array_push($result, [
                    'id' => $id,
                    'src' => url('/storage/' . $image->path)
                ]);
            }
        }
        return $result;
    }

    public static function delete_images_by_ides($ides){
        foreach ($ides as $id) {
            Storage::disk('public')->delete(Image::find($id)->path);
        }
    }
}
