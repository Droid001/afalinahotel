<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use HasFactory;

    static public function add_hotel_room_meta($id)
    {
            $meta = new Meta();
            $meta->name = $id;
            $meta->save();
    }

    static public function delete_hotel_room_meta($id)
    {
        $meta = Meta::where('name', $id);
        if (!empty($meta))
            $meta->delete();
    }
}
