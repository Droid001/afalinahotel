<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HotelRoom extends Model
{
    use HasFactory;

    static public function get_hotel_room_bu_id($id){
        return HotelRoom::find($id);
    }
}
