<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meta;

class MetaController extends Controller
{
    public function get(){
        return Meta::all();
    }

    public function  edit(Request $request){
        $name = $request->input('name');
        $meta = Meta::where('name', $name)->first();
        if(empty($meta)) return;

        if(!empty($request->input('title')))
            $meta->title = $request->input('title');

        if(!empty($request->input('description')))
            $meta->description = $request->input('description');

        if(!empty($request->input('keywords')))
            $meta->keywords = $request->input('keywords');

        if(!empty($request->input('robots')))
            $meta->robots = $request->input('robots');

        $meta->save();
    }
}
