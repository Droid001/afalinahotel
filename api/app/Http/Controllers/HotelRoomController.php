<?php

namespace App\Http\Controllers;

use App\Models\HotelRoom;
use App\Models\Image;
use App\Models\Meta;
use Illuminate\Http\Request;

class HotelRoomController extends Controller
{
    public function get(Request $request)
    {
        $hotelRooms = HotelRoom::all();

        foreach ($hotelRooms as $item) {
            if (!empty($item->img)) {
                $item->img = Image::get_images([$item->img])[0];
            } else {
                $item->img = [
                    'id' => 0,
                    'src' => ''
                ];
            }
            //features
            $item->features = unserialize($item->features);
            $featuresList = [];
            if (!empty($item->features)) {
                foreach ($item->features as $feature) {
                    array_push($featuresList, [
                        'title' => $feature['title'],
                        'description' => $feature['description'],
                        'img' => Image::get_images([$feature['img']])[0]
                    ]);
                };
            }
            $item->features = $featuresList;

            $item->pricies = unserialize($item->pricies);
            $item->busy_dates = ' ';
            $item->gallery = Image::get_images(explode(' ', $item->gallery));
            $item->slides = Image::get_images(explode(' ', $item->slides));
        };

        return $hotelRooms;
    }

    public function add(Request $request)
    {
        $hotelRoom = new HotelRoom();
        $hotelRoom->title = $request->input('title');
        $hotelRoom->intro = $request->input('intro');
        $hotelRoom->description = $request->input('description');
        $hotelRoom->busy_dates = $request->input('busy_dates');

        $hotelRoom->img = $request->input('img');
        $hotelRoom->gallery = implode(' ', $request->input('gallery'));
        $hotelRoom->slides = implode(' ', $request->input('slides'));

        $hotelRoom->pricies = serialize($request->input('pricies'));
        $hotelRoom->features = serialize($request->input('features'));
        $hotelRoom->save();

        Meta::add_hotel_room_meta($hotelRoom->id);
    }

    public function edit(Request $request)
    {
        $hotelRoom = HotelRoom::find($request->id);
        if (!empty($hotelRoom)) {
            $hotelRoom->title = $request->input('title');
            $hotelRoom->intro = $request->input('intro');
            $hotelRoom->description = $request->input('description');
            $hotelRoom->busy_dates = $request->input('busy_dates');

            $hotelRoom->img = $request->input('img');
            $hotelRoom->gallery = implode(' ', $request->input('gallery'));
            $hotelRoom->slides = implode(' ', $request->input('slides'));

            $hotelRoom->pricies = serialize($request->input('pricies'));
            $hotelRoom->features = serialize($request->input('features'));
            $hotelRoom->save();
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $hotelRoom = HotelRoom::find($id);

        if (!empty($hotelRoom)) {
            Image::get_images([$hotelRoom->img]);
            Image::get_images(explode(' ', $hotelRoom->slides));
            Image::get_images(explode(' ', $hotelRoom->gallery));
            Meta::delete_hotel_room_meta($hotelRoom->id);
            $hotelRoom->delete();
        }
    }
}
