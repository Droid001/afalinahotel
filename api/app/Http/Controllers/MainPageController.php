<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MainPage;
use App\Models\Image;

class MainPageController extends Controller
{
    public function get(Request $request){
        $data= MainPage::find(1);
        $slides_ides = explode(' ', $data->slides);
        $slides = Image::get_images($slides_ides);

        return [
            'title'=>$data->title,
            'description'=>$data->description,
            'slides'=>$slides
        ];
    }

    public function edit(Request $request){
        $page = MainPage::find(1);
        if(empty($page)) return;

        if(!empty($request->input('title'))){
            $page->title = $request->input('title');
        }
        if(!empty($request->input('description'))){
            $page->description = $request->input('description');
        }
        if(!empty($request->input('slides'))){
            $buffer = implode(' ', $request->input('slides'));
            $page->slides = $buffer;
        }
        $page->save();
    }
}
