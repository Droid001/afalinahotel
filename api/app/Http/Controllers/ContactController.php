<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function get()
    {
        return Contact::all();
    }

    public function getById(Request $request){
        return Contact::find($request->input('id'));
    }

    public function add(Request $request)
    {
        $title = $request->input('title');
        $text = $request->input('text');

        if (!empty($title && !empty($text))) {
            $contact = new Contact();
            $contact->title = $title;
            $contact->text = $text;
            $contact->save();
        }
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');
        $title = $request->input('title');
        $text = $request->input('text');
        $contact = Contact::find($id);
        if (!empty($contact)) {
            if (!empty($title) && !empty($text)) {
                $contact->title = $title;
                $contact->text = $text;
                $contact->save();
            }
            else{
                report('invalid data');
            }
        }
        else{
            report('invalid id');
        }
    }

    public function delete(Request $request){
        $id = $request->input('id');
        $contact = Contact::find($id);
        if(!empty($contact)){
            $contact->delete();
        }
    }
}
