<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\HotelRoom;
use Illuminate\Http\Request;
use App\Services\TelegramBot;

class FeedbackController extends Controller
{
    public function get(Request $request)
    {
        $validation = $request->validate([
            'type' => 'required|min:3|max:25',
            'name' => 'required|min:3|max:50',
            'tel' => 'required|min:5|max:20',
            'message' => 'max:500',
            'date' => 'required|min:3|max:100',
            'dateIn' => 'max:100',
            'dateOut' => 'max:100',
        ]);

        $feedback = new Feedback();
        $feedback->type = $request->input('type');
        if (!empty($request->input('hotelRoomType')))
            $feedback->hotelRoomType
                = HotelRoom::get_hotel_room_bu_id($request->input('hotelRoomType'))->title;
        $feedback->name = $request->input('name');
        $feedback->tel = $request->input('tel');
        $feedback->message = $request->input('message');
        $feedback->date = $request->input('date');
        $feedback->dateIn = $request->input('dateIn');
        $feedback->dateOut = $request->input('dateOut');
        $feedback->save();

        return TelegramBot::init($feedback);

    }
}
