<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function get(Request $request)
    {
        $ides = explode(' ', $request->input('ides'));
        $result = [];
        foreach ($ides as $id) {
            array_push($result, [
                'id' => $id,
                'src' => url('/storage/' . Image::find($id)->path)
            ]);
        }

        return $result;
    }

    public function add(Request $request)
    {
        $image = $request->file('image');
        if (empty($image)) {
            return null;
        };
        $path = Storage::disk('public')->put('/images/' . date('d.m.Y'), $image);
        $db = new Image();
        $db->path = $path;
        $db->save();
        return $db->id;
    }

    public function delete(Request $request)
    {
        //return $request->input('ides');
        $list_of_ides = explode(' ', $request->input('ides'));
        foreach ($list_of_ides as $id) {
            $image = Image::find($id);
            if (!empty($image)) {
                Storage::disk('public')->delete($image->path);
                $image->delete();
            }
        }
    }
}
