<?php

namespace App\Http\Controllers;

use App\Models\Layout;
use Illuminate\Http\Request;

class LayoutController extends Controller
{
    public function get(Request $request)
    {
        $layout = Layout::find(1);
        $logo_name = $layout->logo_name;
        $logo_tel = $layout->logo_tel;

        return [
            'navbar' => [
                'logo' => [
                    'name' => $logo_name,
                    'url' => ''
                ],
                'tel' => $logo_tel,
                'links' =>[
                    [
                        'name'=>'Контакты',
                        'url' =>'/contacts'
                    ],
//                    [
//                        'name'=>'О нас',
//                        'url' =>'/about'
//                    ]
                ]
            ]
        ];

    }

    public function edit(Request $request){
        $layout = Layout::find(1);
        $layout->logo_name = $request->input('navbar')['logo']['name'];
        $layout->logo_tel = $request->input('navbar')['tel'];
        $layout->save();

        return [
            'navbar' => [
                'logo' => [
                    'name' => $layout->logo_name,
                    'url' => 'index'
                ],
                'tel' => $layout->logo_tel,
                'links' =>[
                    [
                        'name'=>'Контакты',
                        'url' =>'/contacts'
                    ],
                    /*
                    [
                        'name'=>'О нас',
                        'url' =>'/about'
                    ]
                    */
                ]
            ]
        ];
    }
}
