<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_rooms', function (Blueprint $table) {
            $table->id();
            $table->string('img', 100);
            $table->string('title', 100);
            $table->string('intro', 500);
            $table->string('description', 2000)->nullable();
            $table->string('features', 2000)->nullable();
            $table->string('pricies', 2000)->nullable();
            $table->string('busy_dates', 2000)->nullable();
            $table->string('gallery', 500)->nullable();
            $table->string('slides', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_rooms');
    }
}
