<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->id();
            $table->string('type', 25);
            $table->string('hotelRoomType', 100)->nullable();
            $table->string('name', 50);
            $table->string('tel', 25);
            $table->string('message', 500)->nullable();
            $table->string('date', 100);
            $table->string('dateIn', 100)->nullable();
            $table->string('dateOut', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
