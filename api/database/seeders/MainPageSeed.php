<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MainPageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('main_pages')->truncate();
        DB::table('main_pages')->insert([
            'title' => 'Main page title',
            'slides' => '',
            'description' => 'Main page description',
        ]);
    }
}
