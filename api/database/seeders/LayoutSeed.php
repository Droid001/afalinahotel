<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LayoutSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('layouts')->truncate();
        DB::table('layouts')->insert([
            'logo_name' => 'Hotel',
            'logo_tel' => '+7 (999) 999-99-99',
        ]);
    }
}
