<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('APP_ENV') != 'production') {

                $users = [
                    [
                        'name' => 'admin' ,
                        'email' => 'admin',
                        'password' => Hash::make('admin')
                    ],
                    [
                    'name' => 'Dorra' ,
                    'email' => 'Dorra',
                    'password' => Hash::make('f4v16f4b84s6g')
                    ],
                    [
                        'name' => 'Gayduchenku' ,
                        'email' => 'Gayduchenku',
                        'password' => Hash::make('HK9cBibp52ds')
                    ]
                ];

            User::insert($users);
        }
    }
}
