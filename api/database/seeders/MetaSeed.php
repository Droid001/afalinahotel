<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MetaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metas')->truncate();
        DB::table('metas')->insert([
            'name' => 'index',
            'title' => 'main page meta title',
            'description' => 'main page meta description',
            'keywords' => 'main page meta keywords',
            'robots' => 'main page meta robots',
        ]);
        DB::table('metas')->insert([
            'name' => 'contacts',
            'title' => 'contacts page meta title',
            'description' => 'contacts page meta description',
            'keywords' => 'contacts page meta keywords',
            'robots' => 'contacts page meta robots',
        ]);
        DB::table('metas')->insert([
            'name' => 'about',
            'title' => 'about page meta title',
            'description' => 'about page meta description',
            'keywords' => 'about page meta keywords',
            'robots' => 'about page meta robots',
        ]);
    }
}
